$(document).ready(function(){
    makeBoard();
    makeRowInput();
    makeColInput();

    $('#row').change(function() {
        $('#tileContainer').empty();
        makeBoard();

        $("#colInputContainer").empty();
        makeColInput();
    })

    $('#column').change(function() {
        $('#tileContainer').empty();
        makeBoard();

        $("#rowInputContainer").empty();
        makeRowInput();
    })

    $('#groupSize').change(function() {
        $('#tileContainer').empty();
        makeBoard();

        $("#colInputContainer").empty();
        makeColInput();

        $("#rowInputContainer").empty();
        makeRowInput();
    })
})

function makeBoard() {
    var row = $('#row').val();
    var col = $('#column').val();
    
    for(let i = 1; i <= col; i++) {
        $('#tileContainer').append('<div class="rowContainer" id="row' + i + '"></div>')
        for( let j = 1; j <= row; j++) {
            $('#row'+ i).append('<div id="tile' + i + j + '" class="tile"></div>')
        }
    }
}

function makeRowInput() {
    var col = $('#column').val();
    for(let i = 1; i <= col; i++) {
        $("#rowInputContainer").append('<div class="oneRowInputContainer" id="oneRowInputContainer' + i + '"></div>')
        makeInput("#oneRowInputContainer" + i)
    }
}

function makeColInput() {
    var row = $('#row').val();
    for(let i = 1; i <= row; i++) {
        $('#colInputContainer').append('<div class="oneColInputContainer" id="oneColInputContainer' + i + '"></div>')
        makeInput("#oneColInputContainer" + i)
    }
}

function makeInput(idInputContainer) {
    var groupSize = $('#groupSize').val();
    for( let i = 1; i <= groupSize; i++) {
        $(idInputContainer).append('<input class="input" type="text" value="0">')
    }
}

function getRawInput(number, side) {
    var result = "[";
    var groupSize = $('#groupSize').val();
    for(let i = 1; i <= number; i++) {
        var currentLineInput = "["
        var currentLine = ""
        if(side == "row") {
            currentLine = $('#oneRowInputContainer' + i).children()
        } else if (side == "column") {
            currentLine = $('#oneColInputContainer' + i).children()
        }
        for(let j = 1; j <= groupSize; j++) {
            if(j == groupSize) {
                currentLineInput = currentLineInput + currentLine.val() + "]"
            } else {
                currentLineInput = currentLineInput + currentLine.val() + ","
                currentLine = currentLine.next()
            }
        }
        if(i == number) {
            result = result + currentLineInput + "]"
        } else {
            result = result + currentLineInput + ","
        }
    }
    console.log(result)
    return result
}

function getInput(rawInput) {
    var result = ""
    for(let i = 0; i < rawInput.length; i++) {
        var char = rawInput.charAt(i);
        if(char == ",") {
            if((rawInput.charAt(i-1) != "0") && (rawInput.charAt(i-1) != "[")) {
                result = result + char
            }
        } else if(char == "0" && rawInput.charAt(i-1) != "[" && rawInput.charAt(i-1) != ",") {
            result = result + char
        } else if(char != "0") {
            result = result + char
        }
    }
    console.log(result)
    return result
}

function showAnswer(formattedAnswer) {
    console.log(formattedAnswer)
    var answerWithoutVar = formattedAnswer.slice(4, (formattedAnswer.length - 1))
    console.log(answerWithoutVar)
    var arrayedAnswer = getArrayedAnswer(answerWithoutVar)
    console.log(arrayedAnswer)
    $('div').removeClass('active')
    for(let i=0; i < arrayedAnswer.length; i++) {
        if(arrayedAnswer[i][0] != "") {
            for(let j=0; j < arrayedAnswer[i].length; j++) {
                $('#tile'+ (i+1) + (parseInt(arrayedAnswer[i][j]))).addClass("active")
            }
        }
    }
    showFeedbackSolved()
}

function getArrayedAnswer(answerWithoutVar) {
    var result = []
    var containerParsedString = ""
    var answerWithoutParenthess = answerWithoutVar.slice(1, (answerWithoutVar.length - 1))
    for(let i = 0; i < answerWithoutParenthess.length; i++) {
        var char = answerWithoutParenthess.charAt(i);
        if(char == ",") {
            if(answerWithoutParenthess.charAt(i-1) == "]") {
               containerParsedString = containerParsedString + ";"
            } else {
               containerParsedString = containerParsedString + char
            }
        } else {
           containerParsedString = containerParsedString + char
        }
    }
    containerParsedString = containerParsedString.split(";")
    console.log(containerParsedString)
    for(let element of containerParsedString) {
        var elementWithoutParenthess = element.slice(1, (element.length - 1))
        var arrayedElement = elementWithoutParenthess.split(',')
        result.push(arrayedElement)
    }
    
    return result
}

function showLoading() {
    $('#feedback').remove()
    $('#boardContainer').append("<div id='loading'><div id='loader'></div><p>Please wait...<br>This may take a few moments.</p></div>");
    $('#loading').insertAfter('#board')
}

function showFeedbackSolved() {
    $('#loading').remove()
    $('#boardContainer').append("<div id='feedback' class='solved'><p>Done!!</p></div>")
    $('#feedback').insertAfter('#board')
}

function showFeedbackFailed() {
    $('#loading').remove()
    $('#boardContainer').append("<div id='feedback' class='fail'><p>We fail to solve this nonogram, <br>maybe your inputs are incorrect?</p></div>")
    $('#feedback').insertAfter('#board')
}

function showFeedbackError() {
    $('#loading').remove()
    $('#boardContainer').append("<div id='feedback' class='fail'><p>We can't solve this nonogram, <br>maybe your inputs are incorrect?</p></div>")
    $('#feedback').insertAfter('#board')
}

function showFeedbackLimit() {
    $('#loading').remove()
    $('#boardContainer').append("<div id='feedback' class='fail'><p>Sorry<br>we can't solve this nonogram :(</p></div>")
    $('#feedback').insertAfter('#board')
}

function checkInputValue(type, elementId) {
    var inputValue = $(elementId).val();
    if(type == "boardSize") {
        if(inputValue < 2) {
            $(elementId).val(2);
        } else if (inputValue > 10) {
            $(elementId).val(10);
        }
    } else if(type == "groupSize") {
        if(inputValue < 1) {
            $(elementId).val(1);
        } else if (inputValue > 5) {
            $(elementId).val(5);
        }
    }
}
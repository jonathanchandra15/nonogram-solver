from django.shortcuts import render
from django.http import HttpResponseRedirect

def home(request):
    return render(request, 'home.html')

def profile(request):
    return HttpResponseRedirect("https://jonathanjocan.herokuapp.com/")

    